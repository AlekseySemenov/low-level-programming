#include <malloc.h>
#include "ast.h"

Value *create_int(int64_t val) {
    Value *pv = malloc(sizeof(Value));
    pv->type = INTEGER_;
    pv->int_val = val;
    return pv;
}

Value *create_float(float val) {
    Value *pv = malloc(sizeof(Value));
    pv->type = FLOAT_;
    pv->float_val = val;
    return pv;
}

Value *create_bool(bool val) {
    Value *pv = malloc(sizeof(Value));
    pv->type = BOOL_;
    pv->bool_val = val;
    return pv;
}

Value *create_str(char *val) {
    Value *pv = malloc(sizeof(Value));
    pv->type = STRING_;
    pv->str_val = val;
    return pv;
}

void free_value(Value *val) {
    if (val->type == STRING_) {
        free(val->str_val);
    }
    free(val);
}

Property *create_property(char *name, Value *val) {
    Property *property = malloc(sizeof(Property));
    property->name = name;
    property->val = val;
    property->next = NULL;
    return property;
}

PropertyList *create_property_list(Property *property) {
    PropertyList *prop_list = malloc(sizeof(PropertyList));
    prop_list->head = property;
    prop_list->tail = property;
    return prop_list;
}

void add_next_property(PropertyList *prop_list, Property *property) {
    prop_list->tail->next = property;
    prop_list->tail = property;
}

GraphNode *create_graph_node(char *name, char *label, PropertyList *property) {
    GraphNode *node = malloc(sizeof(GraphNode));
    node->name = name;
    node->label = label;
    node->properties = property->head;
    node->next = NULL;
    return node;
}

GraphNodeList *create_graph_node_list(GraphNode *node) {
    GraphNodeList *list = malloc(sizeof(GraphNodeList));
    list->head = node;
    list->tail = node;
    return list;
}

void add_next_node(GraphNodeList *list, GraphNode *node) {
    list->tail->next = node;
    list->tail = node;
}

CmpOperand *create_cmp_value_operand(Value *val) {
    CmpOperand *op = malloc(sizeof(CmpOperand));
    op->type = CONST;
    op->val = val;
    return op;
}

CmpOperand *create_cmp_field_operand(char *field) {
    CmpOperand *op = malloc(sizeof(CmpOperand));
    op->type = FIELD;
    op->field_name = field;
    return op;
}

CmpOperation *create_cmp_operation(CmpOperand *left, CmpOperand *right, CmpOperationType cmp_op) {
    CmpOperation *op = malloc(sizeof(CmpOperation));
    op->left = left;
    op->right = right;
    op->cmp_op = cmp_op;
    return op;
}

Condition *create_condition(CmpOperation *op) {
    Condition *cond = malloc(sizeof(Condition));
    cond->op = op;
    cond->log_op = NULL;
    return cond;
}

LogicOperation *create_logic_operation(LogicOperationType type) {
    LogicOperation *op = malloc(sizeof(LogicOperation));
    op->log_op_type = type;
    op->next = NULL;
    return op;
}

ConditionList *create_condition_list(Condition *cond) {
    ConditionList *list = malloc(sizeof(ConditionList));
    list->head = cond;
    list->tail = cond;
    return list;
}

void add_next_condition(ConditionList *list, Condition *cond, LogicOperationType type) {
    LogicOperation *op = create_logic_operation(type);
    op->next = cond;
    list->tail->log_op = op;
    list->tail = cond;
}

VarField *create_var_field(char *var_name, char *field_name) {
    VarField *field = malloc(sizeof(VarField));
    field->var_name = var_name;
    field->field_name = field_name;
    return field;
}

ReturnValue *create_field_return_value(VarField *field) {
    ReturnValue *ret = malloc(sizeof(ReturnValue));
    ret->type = FIELD_VALUE;
    ret->field = field;
    return ret;
}

ReturnValue *create_graph_return_value(char *node) {
    ReturnValue *ret = malloc(sizeof(ReturnValue));
    ret->type = NODE_VALUE;
    ret->graph_node_name = node;
    return ret;
}

ReturnValue *create_const_return_value(Value *val) {
    ReturnValue *ret = malloc(sizeof(ReturnValue));
    ret->type = CONST_VALUE;
    ret->val = val;
    return ret;
}

ReturnValueList *create_return_value_list(ReturnValue *val) {
    ReturnValueList *list = malloc(sizeof(ReturnValueList));
    list->head = val;
    list->tail = val;
    return list;
}

void add_next_return_value(ReturnValueList *list, ReturnValue *val) {
    list->tail->next = val;
    list->tail = val;
}

void set_create_query_params(Query *query, GraphNodeList *nodes) {
    query->type = CREATE_;
    query->create.nodes = nodes->head;
}

void set_match_query_params(Query *query, char *var, char *label, ConditionList *cond_list, ReturnValueList *ret_list) {
    query->type = MATCH_;
    query->match.var = var;
    query->match.label = label;
    if (cond_list)
        query->match.conditions = cond_list->head;
    else
        query->match.conditions = NULL;
    query->match.values = ret_list->head;
}

void set_delete_query_params(Query *query, char *var, char *label, ConditionList *cond_list) {
    query->type = DELETE_;
    query->delete.var = var;
    query->delete.label = label;
    if (cond_list)
        query->delete.conditions = cond_list->head;
    else
        query->delete.conditions = NULL;
}

void set_set_query_params(Query *query, char *var, char *label, ConditionList *cond_list, PropertyList *prop_list){
    query->type = SET_;
    query->set.var = var;
    query->set.label = label;
    if (cond_list)
        query->set.conditions = cond_list->head;
    else
        query->set.conditions = NULL;
    if (prop_list)
        query->set.properties = prop_list->head;
    else
        query->set.properties = NULL;
}