#ifndef LLP2_AST_H
#define LLP2_AST_H

#include <stdio.h>
#include <stdbool.h>
#include "inttypes.h"

typedef enum {
    INTEGER_,
    STRING_,
    FLOAT_,
    BOOL_,
} ValueType;

typedef enum {
    EQ_, NEQ_, GT_, GTE_, LT_, LTE_, CONTAINS_
} CmpOperationType;

typedef enum {
    OR_, AND_
} LogicOperationType;

typedef enum {
    NODE_VALUE, FIELD_VALUE, CONST_VALUE
} ReturnValueType;

typedef enum {
    FIELD, CONST
} CmpOperandType;

typedef enum {
    CREATE_, MATCH_, SET_, DELETE_
} QueryType;

typedef struct {
    ValueType type;
    union {
        int64_t int_val;
        float float_val;
        bool bool_val;
        char *str_val;
    };
} Value;

typedef struct Property {
    char *name;
    Value *val;
    struct Property *next;
} Property;

typedef struct {
    Property *head;
    Property *tail;
} PropertyList;

typedef struct GraphNode {
    char *name;
    char *label;
    Property *properties;
    struct GraphNode *next;
} GraphNode;

typedef struct {
    GraphNode *head;
    GraphNode *tail;
} GraphNodeList;

typedef struct {
    CmpOperandType type;
    union {
        Value *val;
        char *field_name;
    };
} CmpOperand;

typedef struct LogicOperation LogicOperation;

typedef struct {
    CmpOperand *left;
    CmpOperand *right;
    CmpOperationType cmp_op;
} CmpOperation;

typedef struct Condition {
    CmpOperation *op;
    struct LogicOperation *log_op;
} Condition;

typedef struct LogicOperation {
    LogicOperationType log_op_type;
    Condition *next;
} LogicOperation;


typedef struct {
    Condition *head;
    Condition *tail;
} ConditionList;

typedef struct {
    char *var_name;
    char *field_name;
} VarField;

typedef struct ReturnValue {
    ReturnValueType type;
    union {
        char *graph_node_name;
        VarField *field;
        Value *val;
    };
    struct ReturnValue *next;
} ReturnValue;

typedef struct {
    ReturnValue *head;
    ReturnValue *tail;
} ReturnValueList;

typedef struct {
    GraphNode *nodes;
} CreateQuery;

typedef struct {
    char *var;
    char *label;
    Condition *conditions;
    Property *properties;
} SetQuery;

typedef struct {
    char *var;
    char *label;
    Condition *conditions;
} DeleteQuery;

typedef struct {
    char *var;
    char *label;
    Condition *conditions;
    ReturnValue *values;
} MatchQuery;

typedef struct {
    QueryType type;
    union {
        CreateQuery create;
        MatchQuery match;
        DeleteQuery delete;
        SetQuery set;
    };
} Query;


Value *create_int(int64_t val);
Value *create_float(float val);
Value *create_bool(bool val);
Value *create_str(char *val);

Property *create_property(char *name, Value *val);
PropertyList *create_property_list(Property *property);
void add_next_property(PropertyList *prop_list, Property *property);

GraphNode *create_graph_node(char *name, char *label, PropertyList *property);
GraphNodeList *create_graph_node_list(GraphNode *node);
void add_next_node(GraphNodeList *list, GraphNode *node);

CmpOperand *create_cmp_value_operand(Value *val);
CmpOperand *create_cmp_field_operand(char *field);
CmpOperation *create_cmp_operation(CmpOperand *left, CmpOperand *right, CmpOperationType cmp_op);
Condition *create_condition(CmpOperation *op);
ConditionList *create_condition_list(Condition *cond);
void add_next_condition(ConditionList *list, Condition *cond, LogicOperationType type);

VarField *create_var_field(char *var_name, char *field_name);
ReturnValue *create_field_return_value(VarField *field);
ReturnValue *create_graph_return_value(char *node);
ReturnValue *create_const_return_value(Value *val);
ReturnValueList *create_return_value_list(ReturnValue *val);
void add_next_return_value(ReturnValueList *list, ReturnValue *val);

void set_create_query_params(Query *query, GraphNodeList *nodes);
void set_match_query_params(Query *query, char *var, char *label, ConditionList *cond_list, ReturnValueList *ret_list);
void set_delete_query_params(Query *query, char *var, char *label, ConditionList *cond_list);
void set_set_query_params(Query *query, char *var, char *label, ConditionList *cond_list, PropertyList *prop_list);

#endif //LLP2_AST_H
