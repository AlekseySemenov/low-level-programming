%code requires {
    #include "printer.h"
    #include "ast.h"
    extern Query q;

    extern int yylex();
    void yyerror(const char* err);
}

%union{
    bool boolean;
    int integer;
    float float_val;
    char* string;

    Value *prop_value;
    Property *prop;
    PropertyList *prop_list;
    GraphNode *g_node;
    GraphNodeList *g_node_list;
    Condition *cond;
    ConditionList *cond_list;
    CmpOperationType cmp_op;
    LogicOperationType log_op_type;
    QueryType qt;
    VarField *field;
    ReturnValue *ret_value;
    ReturnValueList *ret_list;
}

%token <qt> MATCH CREATE SET DELETE
%token RETURN WHERE
%token <cmp_op> EQ NEQ GT GTE LT LTE CONTAINS
%token <log_op_type> OR AND
%token DOT COMMA COLON LB RB LBF RBF EOL
%token <string> STRING WORD
%token <integer> INT
%token <boolean> BOOLEAN
%token <float_val> FLOAT

%type <prop_value> value
%type <prop> property
%type <prop_list> properties property_part
%type <g_node> node
%type <g_node_list> nodes
%type <cond> condition
%type <cond_list> conditions where
%type <log_op_type> log_op
%type <cmp_op> cmp_op
%type <ret_value> return_value
%type <ret_list> ret_val_list return

%{
    Query q = {0};
%}


%%

end: query EOL { return 0; }

query: create_query | match_query | delete_query | set_query

create_query: CREATE nodes { set_create_query_params(&q, $2); }

match_query:
    MATCH LB WORD COLON WORD RB where return { set_match_query_params(&q, $3, $5, $7, $8); } |
    MATCH LB WORD COLON WORD RB return {set_match_query_params(&q, $3, $5, NULL, $7); } |
    MATCH LB WORD RB where return { set_match_query_params(&q, $3, NULL, $5, $6); } |
    MATCH LB WORD RB return { set_match_query_params(&q, $3, NULL, NULL, $5); }

delete_query:
    MATCH LB WORD COLON WORD RB where DELETE { set_delete_query_params(&q, $3, $5, $7); } |
    MATCH LB WORD COLON WORD RB DELETE { set_delete_query_params(&q, $3, $5, NULL); } |
    MATCH LB WORD RB where DELETE { set_delete_query_params(&q, $3, NULL, $5); } |
    MATCH LB WORD RB DELETE { set_delete_query_params(&q, $3, NULL, NULL); }

set_query:
    MATCH LB WORD COLON WORD RB where SET properties { set_set_query_params(&q, $3, $5, $7, $9); } |
    MATCH LB WORD COLON WORD RB SET properties { set_set_query_params(&q, $3, $5, NULL, $8); } |
    MATCH LB WORD RB where SET properties { set_set_query_params(&q, $3, NULL, $5, $7); } |
    MATCH LB WORD RB SET properties { set_set_query_params(&q, $3, NULL, NULL, $6); }


return: RETURN ret_val_list { $$ = $2; }

ret_val_list:
    return_value { $$ = create_return_value_list($1); } |
    ret_val_list COMMA return_value { add_next_return_value($1, $3); }

return_value:
    WORD {
        $$ = create_graph_return_value($1);
    } |
    WORD DOT WORD {
        VarField *field = create_var_field($1, $3);
        $$ = create_field_return_value(field);
    } |
    value { $$ = create_const_return_value($1); }

where: WHERE conditions { $$ = $2; }

conditions:
    condition { $$ = create_condition_list($1); } |
    conditions log_op condition { add_next_condition($1, $3, $2); }

condition:
    WORD DOT WORD cmp_op value {
        CmpOperand *left = create_cmp_field_operand($3);
        CmpOperand *right = create_cmp_value_operand($5);
        CmpOperation *op = create_cmp_operation(left, right, $4);
        $$ = create_condition(op);
    } |
    value cmp_op value {
        CmpOperand *left = create_cmp_value_operand($1);
        CmpOperand *right = create_cmp_value_operand($3);
        CmpOperation *op = create_cmp_operation(left, right, $2);
        $$ = create_condition(op);
    }

log_op: OR { $$ = OR_; } | AND { $$ = AND_;}

cmp_op:
    EQ { $$ = EQ_; } |
    NEQ { $$ = NEQ_; } |
    GT { $$ = GT_; } |
    GTE { $$ = GTE_; } |
    LT { $$ = LT_; } |
    LTE { $$ = LTE_; } |
    CONTAINS { $$ = CONTAINS_; }

nodes:
    node { $$ = create_graph_node_list($1); } |
    nodes COMMA node { add_next_node($1, $3); }

node: LB WORD COLON WORD property_part RB { $$ = create_graph_node($2, $4, $5); }

property_part: LBF properties RBF { $$ = $2; }

properties:
    property { $$ = create_property_list($1); } |
    properties COMMA property { add_next_property($1, $3); }

property:
    WORD COLON value { $$ = create_property($1, $3); } |
    WORD DOT WORD EQ value { $$ = create_property($3, $5); }


value:
    INT { $$ = create_int($1); } |
    BOOLEAN { $$ = create_bool($1); } |
    FLOAT { $$ = create_float($1); } |
    STRING { $$ = create_str($1); }


%%

void yyerror(const char* err){
        fprintf(stderr, "%s\n", err);
}