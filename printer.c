#include "printer.h"

void print_tabs(int count) {
    for (int i = 0; i < count; i++) {
        printf("\t");
    }
}

void print_value(Value *val, int tabs) {
    print_tabs(tabs);
    switch (val->type) {
        case INTEGER_:
            printf("type: int\n");
            print_tabs(tabs);
            printf("value: %ld\n", val->int_val);
            break;
        case STRING_:
            printf("type: string\n");
            print_tabs(tabs);
            printf("value: %s\n", val->str_val);
            break;
        case FLOAT_:
            printf("type: float\n");
            print_tabs(tabs);
            printf("value: %f\n", val->float_val);
            break;
        case BOOL_:
            printf("type: bool\n");
            print_tabs(tabs);
            printf("value: %d\n", val->bool_val);
            break;
    }
}

void print_property(Property *prop, int tabs) {
    print_tabs(tabs);
    printf("property_name: %s\n", prop->name);
    print_value(prop->val, tabs);
    printf("\n");
}

void print_properties(Property *properties, int tabs) {
    Property *curr = properties;
    while (curr != NULL) {
        print_property(curr, tabs);
        curr = curr->next;
    }
}

void print_node(GraphNode *node, int tabs) {
    print_tabs(tabs);
    printf("Node name: %s\n", node->name);
    print_tabs(tabs);
    printf("Node label: %s\n", node->label);

    print_properties(node->properties, tabs + 1);
}

void print_nodes(GraphNode *nodes, int tabs) {
    GraphNode *curr = nodes;
    while (curr != NULL) {
        print_node(curr, tabs);
        curr = curr->next;
    }
}

void print_cmp_operand(CmpOperand *op, int tabs) {
    print_tabs(tabs);
    switch (op->type) {
        case FIELD:
            printf("operand type: node_field\n");
            print_tabs(tabs);
            printf("field_name: %s\n", op->field_name);
            break;
        case CONST:
            printf("operand type: const\n");
            print_value(op->val, tabs);
            break;
    }
}

void print_cmp_operation_type(CmpOperationType type, int tabs) {
    print_tabs(tabs);
    printf("cmp_operation: ");
    switch (type) {
        case EQ_:
            printf("=");
            break;
        case NEQ_:
            printf("!=");
            break;
        case GT_:
            printf(">");
            break;
        case GTE_:
            printf(">=");
            break;
        case LT_:
            printf("<");
            break;
        case LTE_:
            printf("<=");
            break;
        case CONTAINS_:
            printf("contains");
            break;
    }
    printf("\n");
}

void print_cmp_operation(CmpOperation *op, int tabs) {
    print_tabs(tabs);
    printf("left_operand:\n");
    print_cmp_operand(op->left, tabs + 1);
    print_cmp_operation_type(op->cmp_op, tabs);
    print_tabs(tabs);
    printf("right_operand: \n");
    print_cmp_operand(op->right, tabs + 1);

}

void print_logical_operation_type(LogicOperationType type, int tabs) {
    print_tabs(tabs);
    switch (type) {
        case OR_:
            printf("OR");
            break;
        case AND_:
            printf("AND");
            break;
    }
    printf("\n");
}

void print_conditions(Condition *cond, int tabs) {
    Condition *curr = cond;
    while (curr != NULL) {
        print_cmp_operation(curr->op, tabs);
        LogicOperation *log_op = curr->log_op;
        if (log_op != NULL && log_op->next != NULL) {
            print_logical_operation_type(log_op->log_op_type, tabs);
            curr = log_op->next;
        } else {
            curr = NULL;
        }
    }
}

void print_var_field(VarField *field, int tabs){
    print_tabs(tabs);
    printf("node name: %s\n", field->var_name);
    print_tabs(tabs);
    printf("filed_name: %s\n", field->field_name);
}

void print_return_values(ReturnValue *val, int tabs) {
    ReturnValue *curr = val;
    while (curr != NULL) {
        print_tabs(tabs);
        switch (curr->type) {
            case NODE_VALUE:
                printf("value type: node\n");
                print_tabs(tabs);
                printf("node_name: %s\n", curr->graph_node_name);
                break;
            case FIELD_VALUE:
                printf("value type: node_field\n");
                print_var_field(curr->field, tabs);
                break;
            case CONST_VALUE:
                printf("value type: const\n");
                print_value(curr->val, tabs);
                break;
        }
        curr = curr->next;
        printf("\n");
    }
}

void print_create_query(CreateQuery q) {
    printf("Query type: create\n");
    print_nodes(q.nodes, 1);
}

void print_match_query(MatchQuery q) {
    printf("Query type: match\n");
    printf("var name: %s\n", q.var);
    if (q.label)
        printf("label name: %s\n", q.label);
    if (q.conditions) {
        printf("Conditions:\n");
        print_conditions(q.conditions, 1);
    }
    printf("Return values:\n");
    print_return_values(q.values, 1);
}

void print_delete_query(DeleteQuery q){
    printf("Query type: delete\n");
    printf("var name: %s\n", q.var);
    if (q.label)
        printf("label name: %s\n", q.label);
    if (q.conditions) {
        printf("Conditions:\n");
        print_conditions(q.conditions, 1);
    }
}

void print_set_query(SetQuery q){
    printf("Query type: delete\n");
    printf("var name: %s\n", q.var);
    if (q.label)
        printf("label name: %s\n", q.label);
    if (q.conditions){
        printf("Conditions:\n");
        print_conditions(q.conditions, 1);
    }
    printf("Properties:\n");
    print_properties(q.properties, 1);
}

void print_query(Query q) {
    switch (q.type) {
        case CREATE_:
            print_create_query(q.create);
            break;
        case SET_:
            print_set_query(q.set);
            break;
        case MATCH_:
            print_match_query(q.match);
            break;
        case DELETE_:
            print_delete_query(q.delete);
            break;
    }
}

