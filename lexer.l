%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "test.tab.h"
%}

%option noyywrap

%%

"create" { return CREATE; }
"match" { return MATCH; }
"set" { return SET; }
"delete" { return DELETE; }
"where" { return WHERE; }
"return" { return RETURN; }
"and" { return AND; }
"or" { return OR; }
"contains" { return CONTAINS; }

\"[^\"]+\"  { yylval.string = strdup(yytext); return STRING; }
"true" | "false"  { yylval.boolean = yytext[0] == 't' ? 1 : 0; return BOOLEAN; }
"false"     { yylval.boolean = false; return BOOLEAN; }
[_a-zA-Z]+ { yylval.string = strdup(yytext); return WORD; }
[-]?[0-9]+\.[0-9]+  { yylval.float_val = atof(yytext); return FLOAT; }
[-]?[1-9]+[0-9]*  { yylval.integer = atoi(yytext); return INT; }

"." { return DOT; }
"," { return COMMA; }
":" { return COLON; }
"(" { return LB; }
")" { return RB; }
"{" { return LBF; }
"}" { return RBF; }

"="  { return EQ; }
"!=" { return NEQ; }
">" { return GT; }
">=" { return GTE; }
"<" { return LT; }
"<=" { return LTE; }

[ \t\r\n]+

; {return EOL; }

. { printf("Unrecognized character: %s\n", yytext); exit(1); }
