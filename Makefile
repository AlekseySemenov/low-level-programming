build:
	bison -d -Wall -o test.tab.c parser.y
	flex -o test.yy.c lexer.l
	gcc -g main.c printer.c ast.c test.yy.c test.tab.c -o main

debug_build:
	bison -d -Wall -o test.tab.c parser.y
	flex -d -o test.yy.c lexer.l
	gcc -g main.c printer.c ast.c test.yy.c test.tab.c -o main -DYYDEBUG=1